extern crate proc_macro;
use proc_macro::TokenStream;

use quote::quote;
use syn::{
    parse::{Parse, ParseStream, Result},
    parse_macro_input,
    punctuated::Punctuated,
    Ident, Token, Visibility,
};

struct ProblemsEnum {
    name: Ident,
    problems: Punctuated<Ident, Token![,]>,
}

impl Parse for ProblemsEnum {
    fn parse(input: ParseStream) -> Result<Self> {
        let name = input.parse()?;
        let problems = if input.parse::<Token![,]>().is_ok() {
            input.parse_terminated(Ident::parse)?
        } else {
            Punctuated::new()
        };

        Ok(Self { name, problems })
    }
}

#[proc_macro]
pub fn gen_problems(tokens: TokenStream) -> TokenStream {
    let ProblemsEnum { name, problems } = parse_macro_input!(tokens as ProblemsEnum);

    // So we have IntoIter for quote!
    let problem_names = problems.iter().collect::<Vec<_>>();
    let enum_variants = problems
        .iter()
        // Convert to PascalCase
        .map(|problem| {
            let mut convert_upper = true;
            problem
                .to_string()
                .chars()
                .map(|c| {
                    if c == '_' {
                        convert_upper = true;
                        c
                    } else {
                        if convert_upper {
                            convert_upper = false;
                            c.to_ascii_uppercase()
                        } else {
                            c
                        }
                    }
                })
                .collect()
        })
        .collect::<Vec<String>>()
        .into_iter()
        .map(|problem| syn::parse_str(problem.as_str()).unwrap())
        .collect::<Vec<Ident>>();

    let expanded = quote! {
        #(mod #problem_names;)*

        use structopt::clap;
        use crate::problems::problem::{Edition, Problem};

        pub enum #name {
            #(#enum_variants),*
        }

        impl #name {
            pub fn for_day(day: u8) -> Result<Self, clap::Error> {
                match day {
                    #(#problem_names::TargetProblem::DAY => Ok(Self::#enum_variants)),*,
                    _ => Err(clap::Error::with_description(
                        "Wrong day",
                        clap::ErrorKind::InvalidValue,
                    ))
                }
            }
        }

        impl Edition for #name {
            fn execute(&self) {
                let output = match self {
                    #(Self::#enum_variants => #problem_names::TargetProblem::execute()),*
                };
                println!("Part 1: {}\nPart 2: {}", output.0, output.1);
            }
        }
    };
    expanded.into()
}

struct Problem {
    name: Ident,
}

impl Parse for Problem {
    fn parse(input: ParseStream) -> Result<Self> {
        input.parse::<Visibility>()?;
        input.parse::<Token![struct]>()?;
        let name = input.parse()?;
        input.parse::<proc_macro2::TokenStream>()?;

        Ok(Self { name })
    }
}

#[proc_macro_attribute]
pub fn mark_problem(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let item_clone = item.clone();
    let Problem { name } = parse_macro_input!(item_clone as Problem);

    let item = proc_macro2::TokenStream::from(item);
    let expanded = quote! {
        pub use #name as TargetProblem;

        #item
    };
    expanded.into()
}
