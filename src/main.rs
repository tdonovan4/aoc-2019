use std::str::FromStr;

use structopt::{clap, StructOpt};

mod data;
mod intcode_computer;
mod math;
mod problems;

use problems::problem::Edition;

enum Editions {
    Y2019,
    Y2020,
}

impl FromStr for Editions {
    type Err = clap::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "2019" => Ok(Self::Y2019),
            "2020" => Ok(Self::Y2020),
            _ => Err(clap::Error::with_description(
                "Wrong edition year",
                clap::ErrorKind::InvalidValue,
            )),
        }
    }
}

#[derive(StructOpt)]
#[structopt(name = "aoc", about = "I solve Advent of Code problems")]
struct Aoc {
    #[structopt(long = "year", short = "y", default_value = "2020")]
    year: u32,
    day: u8,
}

impl Aoc {
    fn run_problem(&self) -> Result<(), clap::Error> {
        match self.year {
            2019 => {
                problems::edition_2019::Edition2019::for_day(self.day)?.execute();
                Ok(())
            }
            2020 => {
                problems::edition_2020::Edition2020::for_day(self.day)?.execute();
                Ok(())
            }
            _ => Err(clap::Error::with_description(
                "Wrong edition year",
                clap::ErrorKind::InvalidValue,
            )),
        }
    }
}

fn main() -> Result<(), clap::Error> {
    #[cfg(target_os = "windows")]
    ansi_term::enable_ansi_support().unwrap();

    let aoc = Aoc::from_args();
    aoc.run_problem()?;

    Ok(())
}
