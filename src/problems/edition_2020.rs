use aoc_macros::gen_problems;

gen_problems!(Edition2020, problem1, problem2, problem3, problem4, problem5);
