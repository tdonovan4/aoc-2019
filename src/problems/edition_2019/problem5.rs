use crate::intcode_computer::IntcodeComputer;
use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Five;

impl Problem for Five {
    const YEAR: u32 = 2019;
    const DAY: u8 = 5;

    fn solve(input: String) -> (String, String) {
        let mut intcode: Vec<i64> = Vec::new();
        for num in input.trim().split(',') {
            let num = num.parse::<i64>().unwrap();
            intcode.push(num);
        }
        let mut computer_air_conditioner = IntcodeComputer::new(intcode.clone(), Some(vec![1]));
        let value1 = computer_air_conditioner.compute().unwrap();
        let mut computer_radiator = IntcodeComputer::new(intcode, Some(vec![5]));
        let value2 = computer_radiator.compute().unwrap();
        (
            value1.1[value1.1.len() - 1].to_string(),
            value2.1[value2.1.len() - 1].to_string(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 5182797.to_string());
        assert_eq!(output.1, 12077198.to_string());
    }
}
