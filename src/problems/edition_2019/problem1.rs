use std::convert::TryInto;

use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct One;

impl One {
    fn calculate_fuel(values: Vec<u32>) -> (Vec<u32>, u32) {
        let mut sum = 0;
        let mut new_values: Vec<u32> = Vec::new();
        for value in values {
            let num: i32 = (value as f32 / 3.0).trunc() as i32 - 2;
            if num > 0 {
                let num: u32 = num.try_into().unwrap();
                sum += num;
                new_values.push(num);
            }
        }
        (new_values, sum)
    }
}

impl Problem for One {
    const YEAR: u32 = 2019;
    const DAY: u8 = 1;

    fn solve(input: String) -> (String, String) {
        let mut sum1 = 0;
        let mut sum2 = 0;
        let mut modules: Vec<u32> = Vec::new();
        for num in input.trim().lines() {
            let num = num.parse::<f32>().unwrap();
            let num: u32 = (num / 3.0).trunc() as u32 - 2;
            modules.push(num);
            sum1 += num;
        }

        sum2 += sum1;

        while !modules.is_empty() {
            let values = One::calculate_fuel(modules);
            modules = values.0;
            sum2 += values.1;
        }

        (sum1.to_string(), sum2.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 3198599.to_string());
        assert_eq!(output.1, 4795042.to_string());
    }
}
