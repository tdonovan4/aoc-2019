use std::collections::HashMap;

use ansi_term::Colour::{Black, White};

use crate::intcode_computer::{IntcodeComputer, State};
use crate::problems::problem::Problem;

type Pos = (i32, i32);

enum Direction {
    Up,
    Down,
    Right,
    Left,
}

struct CoordSystem {
    max_x: i32,
    min_x: i32,
    max_y: i32,
    min_y: i32,
    system: Option<Vec<Vec<String>>>,
}

impl CoordSystem {
    fn new() -> CoordSystem {
        CoordSystem {
            max_x: 0,
            min_x: 0,
            max_y: 0,
            min_y: 0,
            system: None,
        }
    }

    fn check_boundary(&mut self, point: Pos) {
        if point.0 > self.max_x {
            self.max_x = point.0;
        } else if point.0 < self.min_x {
            self.min_x = point.0;
        }

        if point.1 > self.max_y {
            self.max_y = point.1;
        } else if point.1 < self.min_y {
            self.min_y = point.1;
        }
    }

    fn create_empty(&mut self) {
        let size_x = (self.max_x.abs() + self.min_x.abs() + 1) as usize;
        let size_y = (self.max_y.abs() + self.min_y.abs() + 1) as usize;
        self.system = Some(vec![vec![Black.paint("▮").to_string(); size_x]; size_y]);
    }

    fn add_white(&mut self, points: Vec<Pos>) {
        let system = self.system.as_mut().unwrap();
        for point in points.iter() {
            //println!("{:?}", point);
            system[(point.1 - self.min_y) as usize][(point.0 - self.min_x) as usize] =
                White.paint("▮").to_string();
        }
    }
}

impl core::fmt::Display for CoordSystem {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        let mut string = "\n".to_string();
        for y in self.system.as_ref().unwrap().iter().rev() {
            string += &y.join("");
            string.push('\n');
        }
        write!(fmt, "{}", string)
    }
}

struct Robot {
    computer: IntcodeComputer,
    facing: Direction,
    pos: Pos,
    visited: HashMap<Pos, u32>,
}

impl Robot {
    fn new(intcode: Vec<i64>, start_color: i64) -> Robot {
        Robot {
            computer: IntcodeComputer::new(intcode, Some(vec![start_color])),
            facing: Direction::Up,
            pos: (0, 0),
            visited: HashMap::new(),
        }
    }

    fn turn_and_advance(&mut self, side: u32) {
        match side {
            0 => {
                // Left
                match self.facing {
                    Direction::Up => {
                        self.facing = Direction::Left;
                        self.pos.0 -= 1;
                    }
                    Direction::Down => {
                        self.facing = Direction::Right;
                        self.pos.0 += 1;
                    }
                    Direction::Right => {
                        self.facing = Direction::Up;
                        self.pos.1 += 1;
                    }
                    Direction::Left => {
                        self.facing = Direction::Down;
                        self.pos.1 -= 1;
                    }
                }
            }
            1 => {
                // Right
                match self.facing {
                    Direction::Up => {
                        self.facing = Direction::Right;
                        self.pos.0 += 1;
                    }
                    Direction::Down => {
                        self.facing = Direction::Left;
                        self.pos.0 -= 1;
                    }
                    Direction::Right => {
                        self.facing = Direction::Down;
                        self.pos.1 -= 1;
                    }
                    Direction::Left => {
                        self.facing = Direction::Up;
                        self.pos.1 += 1;
                    }
                }
            }
            _ => unreachable!(),
        }
    }

    fn get_current_color(&self) -> u32 {
        match self.visited.get(&self.pos) {
            Some(color) => *color,
            None => 0,
        }
    }

    fn paint(&mut self) {
        loop {
            match self.computer.compute() {
                State::Wait => {
                    let new_color = self.computer.get_output()[0] as u32;
                    let new_direction = self.computer.get_output()[1] as u32;
                    self.visited.insert(self.pos, new_color);
                    self.computer.clear_output();
                    self.turn_and_advance(new_direction);
                    let current_color = self.get_current_color();
                    self.computer.input(vec![current_color as i64]);
                }
                State::Halt(_) => break,
                State::Running => (),
            }
        }
    }
}

#[aoc_macros::mark_problem]
pub struct Eleven;

impl Problem for Eleven {
    const YEAR: u32 = 2019;
    const DAY: u8 = 11;

    fn solve(input: String) -> (String, String) {
        let mut intcode: Vec<i64> = Vec::new();
        for num in input.trim().split(',') {
            let num = num.parse::<i64>().unwrap();
            intcode.push(num);
        }

        let mut robot_black = Robot::new(intcode.clone(), 0);
        robot_black.paint();

        let mut robot_white = Robot::new(intcode, 1);
        robot_white.paint();

        let mut system = CoordSystem::new();
        let points: Vec<Pos> = robot_white
            .visited
            .iter()
            .filter_map(|p| {
                if *p.1 == 1 {
                    system.check_boundary(*p.0);
                    Some(*p.0)
                } else {
                    None
                }
            })
            .collect();

        system.create_empty();
        system.add_white(points);

        (robot_black.visited.len().to_string(), system.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 1964.to_string());
        // Part 2 is too hard to verify
    }
}
