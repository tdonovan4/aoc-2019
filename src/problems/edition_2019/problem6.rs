use std::collections::HashMap;

use indexmap::IndexSet;

use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Six;

fn count_orbit(map: &HashMap<&str, &str>, obj: &str) -> u32 {
    match map.get(obj) {
        None => 0,
        Some(x) => 1 + count_orbit(map, x),
    }
}

fn walk(map: &HashMap<&str, &str>, obj: &str, mut traject: IndexSet<String>) -> IndexSet<String> {
    match map.get(obj) {
        None => traject,
        Some(x) => {
            traject.insert(x.to_string());
            walk(map, x, traject)
        }
    }
}

impl Problem for Six {
    const YEAR: u32 = 2019;
    const DAY: u8 = 6;

    fn solve(input: String) -> (String, String) {
        let map: HashMap<&str, &str> = input
            .trim()
            .lines()
            .map(|orbit| {
                let vec: Vec<_> = orbit.split(')').collect();
                (vec[1], vec[0])
            })
            .collect();

        let sum: u32 = map.keys().map(|orbit| count_orbit(&map, orbit)).sum();
        let you = walk(&map, "YOU", IndexSet::new());
        let san = walk(&map, "SAN", IndexSet::new());
        let intersection = you.intersection(&san).next().unwrap();
        let transfer =
            you.get_full(intersection).unwrap().0 + san.get_full(intersection).unwrap().0;
        (sum.to_string(), transfer.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 261306.to_string());
        assert_eq!(output.1, 382.to_string());
    }
}
