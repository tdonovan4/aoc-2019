use indexmap::IndexSet;

use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Three;

struct Wire<'a> {
    moves: Vec<&'a str>,
    pos: (i32, i32),
    wire: WireResult,
}

struct WireResult {
    set: IndexSet<(i32, i32)>,
    duplicates: Vec<usize>,
}

impl WireResult {
    fn new() -> WireResult {
        WireResult {
            set: IndexSet::new(),
            duplicates: Vec::new(),
        }
    }

    fn get_real_length(&self, index: usize) -> usize {
        let mut base = index + 1;
        for duplicate in self.duplicates.iter() {
            // Add misssing duplicates to length
            if *duplicate <= index {
                base += 1;
            }
        }
        base
    }
}

impl<'a> Wire<'a> {
    fn new(moves: Vec<&str>) -> Wire {
        Wire {
            moves,
            pos: (0, 0),
            wire: WireResult::new(),
        }
    }

    fn get(mut self) -> WireResult {
        for next_move in self.moves {
            let mut chars = next_move.chars();
            let direction = chars.next().unwrap();
            let steps: i32 = chars.collect::<String>().parse().unwrap();
            for _ in 1..=steps {
                match direction {
                    'L' => {
                        self.pos.0 -= 1;
                    }
                    'R' => {
                        self.pos.0 += 1;
                    }
                    'U' => {
                        self.pos.1 += 1;
                    }
                    'D' => {
                        self.pos.1 -= 1;
                    }
                    _ => panic!("Wrong direction"),
                }
                if !self.wire.set.insert(self.pos) {
                    self.wire.duplicates.push(self.wire.set.len());
                }
            }
        }
        self.wire
    }
}

impl Problem for Three {
    const YEAR: u32 = 2019;
    const DAY: u8 = 3;

    fn solve(input: String) -> (String, String) {
        let mut moves: Vec<Vec<&str>> = Vec::new();
        for wire in input.trim().split('\n') {
            moves.push(wire.split(',').collect());
        }
        let wire1 = Wire::new(moves[0].clone()).get();
        let wire2 = Wire::new(moves[1].clone()).get();
        let mut intersections: Vec<(i32, usize)> = wire1
            .set
            .intersection(&wire2.set)
            .map(|intersect| {
                let index1 = wire1.set.get_full(intersect).unwrap().0;
                let index2 = wire2.set.get_full(intersect).unwrap().0;
                (
                    intersect.0.abs() + intersect.1.abs(),
                    wire1.get_real_length(index1) + wire2.get_real_length(index2),
                )
            })
            .collect();
        intersections.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());
        let first_challenge = intersections[0].0;

        intersections.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        let second_challenge = intersections[0].1;

        (first_challenge.to_string(), second_challenge.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 627.to_string());
        assert_eq!(output.1, 13190.to_string());
    }
}
