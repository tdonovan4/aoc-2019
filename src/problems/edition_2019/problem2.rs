use crate::intcode_computer::IntcodeComputer;
use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Two;

impl Two {
    fn solver(intcode: Vec<i64>) -> String {
        for verb in 0..100 {
            for noun in 0..100 {
                let mut intcode_modified = intcode.clone();
                intcode_modified[1] = noun;
                intcode_modified[2] = verb;
                if IntcodeComputer::new(intcode_modified, None)
                    .compute()
                    .unwrap()
                    .0[0]
                    == 19_690_720
                {
                    return (100 * noun + verb).to_string();
                }
            }
        }
        unreachable!();
    }
}

impl Problem for Two {
    const YEAR: u32 = 2019;
    const DAY: u8 = 2;

    fn solve(input: String) -> (String, String) {
        let mut intcode: Vec<i64> = Vec::new();
        for num in input.trim().split(',') {
            let num = num.parse::<i64>().unwrap();
            intcode.push(num);
        }
        let mut intcode_modified = intcode.clone();
        intcode_modified[1] = 12;
        intcode_modified[2] = 2;
        (
            IntcodeComputer::new(intcode_modified, None)
                .compute()
                .unwrap()
                .0[0]
                .to_string(),
            Two::solver(intcode),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 2890696.to_string());
        assert_eq!(output.1, 8226.to_string());
    }
}
