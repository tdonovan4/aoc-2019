use crate::intcode_computer::IntcodeComputer;
use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Nine;

impl Problem for Nine {
    const YEAR: u32 = 2019;
    const DAY: u8 = 9;

    fn solve(input: String) -> (String, String) {
        let mut intcode: Vec<i64> = Vec::new();
        for num in input.trim().split(',') {
            let num = num.parse::<i64>().unwrap();
            intcode.push(num);
        }
        let mut computer_test = IntcodeComputer::new(intcode.clone(), Some(vec![1]));
        let mut computer = IntcodeComputer::new(intcode, Some(vec![2]));
        (
            computer_test.compute().unwrap().1[0].to_string(),
            computer.compute().unwrap().1[0].to_string(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 2427443564u64.to_string());
        assert_eq!(output.1, 87221.to_string());
    }
}
