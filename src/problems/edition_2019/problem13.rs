use crate::intcode_computer::{IntcodeComputer, State};
use crate::problems::problem::Problem;

use std::{cmp::Ordering, collections::HashSet, ops::Range};

#[derive(Debug, PartialEq)]
enum TileType {
    Empty,
    Wall,
    Block,
    HorizontalPaddle,
    Ball,
}

impl TileType {
    fn new(type_id: u32) -> Self {
        match type_id {
            0 => Self::Empty,
            1 => Self::Wall,
            2 => Self::Block,
            3 => Self::HorizontalPaddle,
            4 => Self::Ball,
            _ => panic!("Wrong tile type"),
        }
    }
}

#[derive(Debug)]
struct Tile {
    x: i32,
    y: i32,
    tile_type: TileType,
}

impl Tile {
    fn new(x: i32, y: i32, type_id: u32) -> Self {
        Self {
            x,
            y,
            tile_type: TileType::new(type_id),
        }
    }
}

#[derive(Debug)]
struct Frame {
    tiles: Vec<Tile>,
    score: u32,
}

impl Frame {
    fn new(tiles_data: &[i64]) -> Self {
        let mut tiles = Vec::new();

        let mut score = 0;

        for tile_data in tiles_data.chunks(3) {
            if tile_data[0] != -1 && tile_data[1] != 0 {
                tiles.push(Tile::new(
                    tile_data[0] as i32,
                    tile_data[1] as i32,
                    tile_data[2] as u32,
                ));
            } else {
                score = tile_data[2] as u32;
            }
        }

        Self { tiles, score }
    }
}

#[derive(Debug)]
struct Game {
    blocks: HashSet<(i32, i32)>,
    bounds: (Range<i32>, Range<i32>),
    paddle: (i32, i32),
    ball: (i32, i32),
    score: u32,
}

impl Game {
    fn modify_range(range: &mut Range<i32>, value: i32) {
        if value < range.start {
            range.start = value;
            range.end -= 1;
        } else if value + 1 > range.end {
            range.end = value + 1;
            range.start -= 1;
        }
    }

    pub fn new(initial_frame: Frame) -> Self {
        let mut game = Self {
            blocks: HashSet::new(),
            bounds: (0..1, 0..1),
            paddle: (0, 0),
            ball: (0, 0),
            score: 0,
        };

        game.add_frame(initial_frame);
        game
    }

    pub fn add_frame(&mut self, frame: Frame) {
        for tile in frame.tiles {
            match tile.tile_type {
                TileType::Empty => {
                    self.blocks.remove(&(tile.x, tile.y));
                }
                TileType::Block => {
                    self.blocks.insert((tile.x, tile.y));
                }
                TileType::Wall => {
                    Self::modify_range(&mut self.bounds.0, tile.x);
                    Self::modify_range(&mut self.bounds.1, tile.y);
                }
                TileType::HorizontalPaddle => self.paddle = (tile.x, tile.y),
                TileType::Ball => self.ball = (tile.x, tile.y),
            }

            self.score = frame.score;
        }
    }

    fn count_blocks(&self) -> usize {
        self.blocks.len()
    }

    fn get_ball_pos(&self) -> (i32, i32) {
        self.ball
    }
}

#[aoc_macros::mark_problem]
pub struct Thirteen;

impl Problem for Thirteen {
    const YEAR: u32 = 2019;
    const DAY: u8 = 13;

    fn solve(input: String) -> (String, String) {
        let mut intcode: Vec<i64> = Vec::new();
        for num in input.trim().split(',') {
            let num = num.parse::<i64>().unwrap();
            intcode.push(num);
        }
        let mut computer1 = IntcodeComputer::new(intcode.clone(), None);
        let game1 = Game::new(Frame::new(&computer1.compute().unwrap().1));

        intcode[0] = 2;
        let mut computer2 = IntcodeComputer::new(intcode, None);
        computer2.compute();
        let mut game2 = Game::new(Frame::new(computer2.get_output()));
        computer2.clear_output();

        loop {
            if let State::Halt((_, output)) = computer2.compute() {
                game2.add_frame(Frame::new(&output));
                break;
            }

            game2.add_frame(Frame::new(computer2.get_output()));
            computer2.clear_output();

            let input = match game2.get_ball_pos().0.cmp(&game2.paddle.0) {
                Ordering::Equal => 0,
                Ordering::Greater => 1,
                Ordering::Less => -1,
            };

            computer2.input(vec![input]);
        }

        assert_eq!(game2.count_blocks(), 0);

        (game1.count_blocks().to_string(), game2.score.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 298.to_string());
        assert_eq!(output.1, 13956.to_string());
    }
}
