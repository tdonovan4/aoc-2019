use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Four;

fn check_password(nums: &[u32], max_same: bool) -> bool {
    let mut previous = None;
    let mut same = false;
    let mut same_streak = 1;
    for num in nums {
        if let Some(previous) = previous {
            if num < previous {
                return false;
            } else if num == previous {
                if max_same {
                    same_streak += 1;
                } else {
                    same = true;
                }
            } else if max_same {
                if same_streak == 2 {
                    same = true;
                }
                same_streak = 1;
            }
        }
        previous = Some(num);
    }
    same || same_streak == 2
}

impl Problem for Four {
    const YEAR: u32 = 2019;
    const DAY: u8 = 4;

    fn solve(input: String) -> (String, String) {
        let bounds: Vec<u32> = input
            .trim()
            .split('-')
            .map(|x| x.parse::<u32>().unwrap())
            .collect();
        let mut valid1: u32 = 0;
        let mut valid2: u32 = 0;
        for i in bounds[0]..=bounds[1] {
            let nums: Vec<u32> = i
                .to_string()
                .chars()
                .filter_map(|x| x.to_digit(10))
                .collect();
            if check_password(&nums, false) {
                valid1 += 1;
            };
            if check_password(&nums, true) {
                valid2 += 1;
            };
        }
        (valid1.to_string(), valid2.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 1605.to_string());
        assert_eq!(output.1, 1102.to_string());
    }
}
