use crate::math;
use crate::problems::problem::Problem;

use std::cell::Cell;
use std::cmp::Ordering;

#[derive(Debug, PartialEq, Clone)]
struct Moon {
    pos: Cell<(i32, i32, i32)>,
    velocities: Cell<(i32, i32, i32)>,
    original_pos: (i32, i32, i32),
}

impl Moon {
    fn new(data: &str) -> Self {
        let mut numbers = Vec::new();
        let mut number_chars = String::new();
        for character in data.chars() {
            if character.is_numeric() || character == '-' {
                number_chars.push(character);
            } else if !number_chars.is_empty() {
                // Number is done
                numbers.push(number_chars.parse().unwrap());
                number_chars.clear();
            }
        }

        Self {
            pos: Cell::new((numbers[0], numbers[1], numbers[2])),
            velocities: Cell::new((0, 0, 0)),
            original_pos: (numbers[0], numbers[1], numbers[2]),
        }
    }

    fn calculate_gravity_one_axis(first: (&i32, &mut i32), second: (&i32, &mut i32)) {
        match first.0.cmp(second.0) {
            Ordering::Less => {
                *first.1 += 1;
                *second.1 -= 1;
            }
            Ordering::Greater => {
                *first.1 -= 1;
                *second.1 += 1;
            }
            _ => (),
        }
    }

    fn calculate_gravity(&self, other: &Self) {
        let self_pos = self.pos.get();
        let mut self_vel = self.velocities.get();
        let other_pos = other.pos.get();
        let mut other_vel = other.velocities.get();

        Self::calculate_gravity_one_axis(
            (&self_pos.0, &mut self_vel.0),
            (&other_pos.0, &mut other_vel.0),
        );
        Self::calculate_gravity_one_axis(
            (&self_pos.1, &mut self_vel.1),
            (&other_pos.1, &mut other_vel.1),
        );
        Self::calculate_gravity_one_axis(
            (&self_pos.2, &mut self_vel.2),
            (&other_pos.2, &mut other_vel.2),
        );

        // Update velocities
        self.velocities.set(self_vel);
        other.velocities.set(other_vel);
    }

    fn calculate_pos(&self) {
        let vel = self.velocities.get();
        let pos = self.pos.get();

        self.pos.set((pos.0 + vel.0, pos.1 + vel.1, pos.2 + vel.2));
    }

    fn total_energy(&self) -> u32 {
        let vel = self.velocities.get();
        let pos = self.pos.get();

        let pot = (vel.0.abs() + vel.1.abs() + vel.2.abs()) as u32;
        let kin = (pos.0.abs() + pos.1.abs() + pos.2.abs()) as u32;
        pot * kin
    }
}

#[derive(Debug, PartialEq, Clone)]
struct System {
    moons: Vec<Moon>,
}

impl System {
    fn new(data: String) -> Self {
        let moons = data
            .split('\n')
            .take_while(|moon_data| !moon_data.is_empty())
            .map(|moon_data| Moon::new(moon_data))
            .collect();

        Self { moons }
    }

    fn get_pairs(&self) -> Vec<(&Moon, &Moon)> {
        if self.moons.len() < 2 {
            Vec::new()
        } else {
            let mut pairs = Vec::new();
            for i in 0..self.moons.len() - 1 {
                let first = &self.moons[i];
                for s in i + 1..self.moons.len() {
                    pairs.push((first, &self.moons[s]));
                }
            }
            pairs
        }
    }

    fn calculate_next_step(&self, pairs: &[(&Moon, &Moon)]) {
        for pair in pairs {
            pair.0.calculate_gravity(pair.1);
        }

        for moon in self.moons.iter() {
            moon.calculate_pos();
        }
    }

    fn calculate(&self, steps: u32) {
        let pairs = self.get_pairs();
        for _ in 0..steps {
            self.calculate_next_step(&pairs);
        }
    }

    fn total_energy(&self) -> u32 {
        let mut total = 0;
        for moon in self.moons.iter() {
            total += moon.total_energy();
        }
        total
    }

    fn copy_system(self) -> u64 {
        let pairs = self.get_pairs();
        let mut x_step = None;
        let mut y_step = None;
        let mut z_step = None;

        let mut steps = 1;

        while x_step.is_none() || y_step.is_none() || z_step.is_none() {
            let mut done_x = 0;
            let mut done_y = 0;
            let mut done_z = 0;

            self.calculate_next_step(&pairs);
            for moon in self.moons.iter() {
                if moon.velocities.get().0 == 0 && moon.pos.get().0 == moon.original_pos.0 {
                    done_x += 1;
                }
                if moon.velocities.get().1 == 0 && moon.pos.get().1 == moon.original_pos.1 {
                    done_y += 1;
                }
                if moon.velocities.get().2 == 0 && moon.pos.get().2 == moon.original_pos.2 {
                    done_z += 1;
                }
            }

            if x_step.is_none() && done_x == self.moons.len() {
                x_step = Some(steps);
            }
            if y_step.is_none() && done_y == self.moons.len() {
                y_step = Some(steps);
            }
            if z_step.is_none() && done_z == self.moons.len() {
                z_step = Some(steps);
            }

            steps += 1
        }

        math::find_lcm(
            math::find_lcm(
                x_step.unwrap_or_default() as u64,
                y_step.unwrap_or_default() as u64,
            ),
            z_step.unwrap_or_default(),
        )
    }
}

#[aoc_macros::mark_problem]
pub struct Twelve;

impl Problem for Twelve {
    const YEAR: u32 = 2019;
    const DAY: u8 = 12;

    fn solve(input: String) -> (String, String) {
        let system = System::new(input);
        let system_two = system.clone();

        system.calculate(1000);

        (
            system.total_energy().to_string(),
            system_two.copy_system().to_string(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 6227.to_string());
        assert_eq!(output.1, 331346071640472u64.to_string());
    }
}
