use crate::intcode_computer::{IntcodeComputer, State};
use crate::problems::problem::Problem;

struct Combinations {
    combinations: Vec<[i64; 5]>,
}

impl Combinations {
    fn new() -> Combinations {
        Combinations {
            combinations: Vec::new(),
        }
    }

    fn get(&mut self, combination: Vec<i64>, left: Vec<i64>) {
        if !left.is_empty() {
            for i in left.iter() {
                let mut left_now = left.clone();
                let mut combination_now = combination.clone();
                left_now.retain(|&x| x != *i);
                combination_now.push(*i);
                self.get(combination_now, left_now)
            }
        } else {
            let mut array = [0; 5];
            array.copy_from_slice(&combination[..5]);
            self.combinations.push(array)
        }
    }

    fn get_every(mut self, numbers: Vec<i64>) -> Vec<[i64; 5]> {
        self.get(Vec::new(), numbers);
        self.combinations
    }
}

struct AmpChain {
    amps: [IntcodeComputer; 5],
}

impl AmpChain {
    fn find_highest(intcode: &[i64]) -> Vec<u32> {
        let mut output: Vec<u32> = Vec::new();
        let combinations = Combinations::new().get_every((0..5).collect::<Vec<i64>>());
        for combination in combinations {
            let mut chain = AmpChain {
                amps: [
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[0]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[1]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[2]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[3]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[4]])),
                ],
            };
            output.push(chain.calculate_output(0));
        }
        output.sort_unstable();
        output
    }

    fn find_highest_feedback(intcode: &[i64]) -> Vec<u32> {
        let mut output: Vec<u32> = Vec::new();
        let combinations = Combinations::new().get_every((5..10).collect::<Vec<i64>>());
        for combination in combinations {
            let mut chain = AmpChain {
                amps: [
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[0]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[1]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[2]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[3]])),
                    IntcodeComputer::new(Vec::from(intcode), Some(vec![combination[4]])),
                ],
            };
            output.push(chain.calculate_output(0));
        }
        output.sort_unstable();
        output
    }

    fn calculate_output(&mut self, mut previous_output: i64) -> u32 {
        let mut next = false;
        for amp in self.amps.iter_mut() {
            amp.input(vec![previous_output]);
            previous_output = match amp.compute() {
                State::Halt(output) => *output.1.last().unwrap(),
                State::Wait => {
                    next = true;
                    *amp.get_output().last().unwrap()
                }
                State::Running => unreachable!(),
            }
        }
        if next {
            self.calculate_output(previous_output)
        } else {
            previous_output as u32
        }
    }
}

#[aoc_macros::mark_problem]
pub struct Seven;

impl Problem for Seven {
    const YEAR: u32 = 2019;
    const DAY: u8 = 7;

    fn solve(input: String) -> (String, String) {
        let mut intcode: Vec<i64> = Vec::new();
        for num in input.trim().split(',') {
            let num = num.parse::<i64>().unwrap();
            intcode.push(num);
        }

        (
            AmpChain::find_highest(&intcode).last().unwrap().to_string(),
            AmpChain::find_highest_feedback(&intcode)
                .last()
                .unwrap()
                .to_string(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 255840.to_string());
        assert_eq!(output.1, 84088865.to_string());
    }
}
