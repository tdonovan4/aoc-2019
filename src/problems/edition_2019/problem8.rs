use ansi_term::Colour::{Black, White};

use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Eight;

impl Problem for Eight {
    const YEAR: u32 = 2019;
    const DAY: u8 = 8;

    fn solve(input: String) -> (String, String) {
        let chunk_size = 25 * 6;
        let layers = input
            .trim()
            .as_bytes()
            .chunks(chunk_size)
            .map(|s| {
                let mut zero = 0;
                let mut one = 0;
                let mut two = 0;
                for byte in s {
                    match *byte as char {
                        '0' => zero += 1,
                        '1' => one += 1,
                        '2' => two += 1,
                        _ => unreachable!(),
                    }
                }
                ((zero, one * two), s)
            })
            .collect::<Vec<_>>();
        let mut corruption_test = layers.iter().map(|x| x.0).collect::<Vec<_>>();
        corruption_test.sort_unstable();

        let mut image = "\n".to_string();
        for i in 0..chunk_size {
            for layer in &layers {
                match layer.1[i] as char {
                    '0' => {
                        image.push_str(&White.paint("▮").to_string());
                        if (i + 1) % 25 == 0 {
                            image.push('\n')
                        }
                        break;
                    }
                    '1' => {
                        image.push_str(&Black.paint("▮").to_string());
                        if (i + 1) % 25 == 0 {
                            image.push('\n')
                        }
                        break;
                    }
                    '2' => (),
                    _ => unreachable!(),
                }
            }
        }

        ((corruption_test[0].1).to_string(), image)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 1690.to_string());
        // Part 2 is too hard to verify
    }
}
