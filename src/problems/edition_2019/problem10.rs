use std::collections::HashSet;

use crate::problems::problem::Problem;

type Pos = (i32, i32);
type TargetInfo = (i32, u32, Pos);

#[aoc_macros::mark_problem]
pub struct Ten;

impl Ten {
    fn get_best_vision(base_locations: &[Pos]) -> (usize, Pos) {
        let mut visible_asteroids_count: Vec<(usize, Pos)> = Vec::new();

        for i in 0..base_locations.len() {
            let possible_base = base_locations[i];
            let mut vision: HashSet<i32> = HashSet::new();
            let mut asteroids = base_locations.to_owned();
            asteroids.remove(i);
            for asteroid in asteroids {
                let y = (asteroid.1 - possible_base.1) as f32;
                let x = (asteroid.0 - possible_base.0) as f32;
                let atan = (x.atan2(y) * 1_000_000.0) as i32;

                vision.insert(atan);
            }
            visible_asteroids_count.push((vision.len(), possible_base));
        }
        visible_asteroids_count.sort_unstable();

        *visible_asteroids_count.last().unwrap()
    }

    fn destroy_side(
        side: Vec<TargetInfo>,
        kill_count: &mut u32,
        two_hundred: &mut Option<Pos>,
    ) -> Vec<TargetInfo> {
        let mut previous_atan: Option<i32> = None;
        let mut new: Vec<TargetInfo> = Vec::new();

        for asteroid_info in side {
            if previous_atan.is_none() || asteroid_info.0 > previous_atan.unwrap() {
                *kill_count += 1;
                previous_atan = Some(asteroid_info.0);
            } else {
                new.push(asteroid_info);
            }

            if *kill_count == 200 {
                *two_hundred = Some(asteroid_info.2);
                break;
            }
        }
        new
    }
}

impl Problem for Ten {
    const YEAR: u32 = 2019;
    const DAY: u8 = 10;

    fn solve(input: String) -> (String, String) {
        let mut asteroids: Vec<Pos> = Vec::new();
        for (y, line) in input.trim().lines().enumerate() {
            for (x, byte) in line.bytes().enumerate() {
                if byte as char == '#' {
                    asteroids.push((x as i32, y as i32));
                }
            }
        }

        let best_asteroid = Ten::get_best_vision(&asteroids);

        let laser_pos = best_asteroid.1;

        let mut right_side: Vec<TargetInfo> = Vec::new();
        let mut left_side: Vec<TargetInfo> = Vec::new();
        let mut other_asteroids = asteroids;
        other_asteroids.retain(|x| *x != laser_pos);

        for target in other_asteroids {
            let x = (target.0 - laser_pos.0) as f32;
            let y = (target.1 - laser_pos.1) as f32;
            // Negative atan so we can use ascending order for atan and distance when sorting
            let atan = -(x.atan2(y) * 1_000_000.0) as i32;
            let distance = (f32::sqrt(x.powi(2) + y.powi(2)) * 1_000_000.0) as u32;

            if atan > 0 {
                left_side.push((atan, distance, target));
            } else {
                right_side.push((atan, distance, target));
            }
        }
        right_side.sort_unstable();
        left_side.sort_unstable();

        let mut kill_count = 0;
        let mut two_hundred: Option<Pos> = None;
        while two_hundred.is_none() {
            right_side = Ten::destroy_side(right_side, &mut kill_count, &mut two_hundred);
            left_side = Ten::destroy_side(left_side, &mut kill_count, &mut two_hundred);
        }
        let two_hundred = two_hundred.unwrap();

        (
            best_asteroid.0.to_string(),
            (two_hundred.0 * 100 + two_hundred.1).to_string(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 260.to_string());
        assert_eq!(output.1, 608.to_string());
    }
}
