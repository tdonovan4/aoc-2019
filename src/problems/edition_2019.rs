use aoc_macros::gen_problems;

gen_problems!(
    Edition2019,
    problem1,
    problem2,
    problem3,
    problem4,
    problem5,
    problem6,
    problem7,
    problem8,
    problem9,
    problem10,
    problem11,
    problem12,
    problem13
);
