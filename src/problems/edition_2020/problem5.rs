use std::ops::Range;

use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Five;

fn range_length(range: &Range<u32>) -> u32 {
    range.end - range.start
}

fn get_seat_id(row: u32, column: u32) -> u32 {
    row * 8 + column
}

fn get_seat_pos(s: &str) -> (u32, u32) {
    let mut row_range = 0..128;
    let mut column_range = 0..8;
    for char in s.chars() {
        match char {
            'F' => row_range.end -= range_length(&row_range) / 2,
            'B' => row_range.start += range_length(&row_range) / 2,
            'R' => column_range.start += range_length(&column_range) / 2,
            'L' => column_range.end -= range_length(&column_range) / 2,
            _ => (),
        }
    }
    let row = row_range.start;
    let column = column_range.start;
    (row, column)
}

impl Problem for Five {
    const YEAR: u32 = 2020;
    const DAY: u8 = 5;

    fn solve(input: String) -> (String, String) {
        // Vector indexed by id, so size is max id + 1
        let mut seats = vec![false; get_seat_id(127, 7) as usize + 1];
        let mut highest_id = 0;
        for line in input.lines() {
            let (row, column) = get_seat_pos(line);
            let id = get_seat_id(row, column);
            if id > highest_id {
                highest_id = id;
            }
            seats[id as usize] = true;
        }

        let mut my_seat_id = None;
        for (id, seat) in seats.iter().enumerate().skip(1) {
            if !seat && seats[id - 1] && seats[id + 1] {
                my_seat_id = Some(id);
                break;
            }
        }

        (highest_id.to_string(), my_seat_id.unwrap().to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 944.to_string());
        assert_eq!(output.1, 554.to_string());
    }
}
