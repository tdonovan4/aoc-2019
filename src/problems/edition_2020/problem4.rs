use std::{convert::TryFrom, num::ParseIntError, str::FromStr};

use crate::problems::problem::Problem;

use thiserror::Error;

#[aoc_macros::mark_problem]
pub struct Four;

#[derive(Error, Debug)]
enum ParsingError {
    #[error("Missing field {0}")]
    MissingField(&'static str),
    #[error("Wrong field {0}")]
    WrongField(String),
    #[error("Error while converting value {0}")]
    ParseIntError(#[from] ParseIntError),
    #[error("Error while parsing {0} with value {1}")]
    ParseError(&'static str, String),
    #[error("Invalid value {0}: {1}")]
    InvalidValue(String, &'static str),
}

#[derive(Debug)]
enum Height {
    Cm(u8),
    Inches(u8),
}

impl FromStr for Height {
    type Err = ParsingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars().peekable();
        let mut i = 0;
        while chars.peek().filter(|c| c.is_numeric()).is_some() {
            chars.next();
            i += 1;
        }

        let size = s[0..i].parse()?;

        let unit = chars.collect::<String>();
        match unit.as_str() {
            "cm" => Ok(Self::Cm(size)),
            "in" => Ok(Self::Inches(size)),
            _ => Err(Self::Err::ParseError("unit of height", unit)),
        }
    }
}

#[derive(Debug)]
struct HairColor {
    color: u32,
}

impl FromStr for HairColor {
    type Err = ParsingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !s.starts_with('#') {
            return Err(Self::Err::InvalidValue(s.to_owned(), "missing leading #"));
        }

        // # + 6 characters
        if s.len() != 7 {
            return Err(Self::Err::InvalidValue(s.to_owned(), "not of length 7"));
        }

        Ok(Self {
            color: u32::from_str_radix(&s[1..], 16)?,
        })
    }
}

#[derive(Debug)]
enum EyeColor {
    Amber,
    Blue,
    Brown,
    Grey,
    Green,
    Hazelnut,
    Other,
}

impl FromStr for EyeColor {
    type Err = ParsingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "amb" => Self::Amber,
            "blu" => Self::Blue,
            "brn" => Self::Brown,
            "gry" => Self::Grey,
            "grn" => Self::Green,
            "hzl" => Self::Hazelnut,
            "oth" => Self::Other,
            _ => return Err(Self::Err::ParseError("eye color", s.to_string())),
        })
    }
}

#[derive(Debug)]
struct Passport {
    /// Birth year
    byr: u16,
    /// Issue year
    iyr: u16,
    /// Expiration year
    eyr: u16,
    /// Height
    hgt: Height,
    /// Hair color
    hcl: HairColor,
    /// Eye color
    ecl: EyeColor,
    /// Passport id
    pid: u32,
    /// Country id
    cid: Option<u16>,
}

impl FromStr for Passport {
    type Err = ParsingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        #[derive(Default)]
        struct PassportBuilder {
            byr: Option<String>,
            iyr: Option<String>,
            eyr: Option<String>,
            hgt: Option<String>,
            hcl: Option<String>,
            ecl: Option<String>,
            pid: Option<String>,
            cid: Option<String>,
        }

        impl PassportBuilder {
            fn add_field(&mut self, field: String, value: String) -> Result<(), ParsingError> {
                match field.as_str() {
                    "byr" => self.byr = Some(value),
                    "iyr" => self.iyr = Some(value),
                    "eyr" => self.eyr = Some(value),
                    "hgt" => self.hgt = Some(value),
                    "hcl" => self.hcl = Some(value),
                    "ecl" => self.ecl = Some(value),
                    "pid" => self.pid = Some(value),
                    "cid" => {
                        self.cid = {
                            if value.is_empty() {
                                None
                            } else {
                                Some(value)
                            }
                        }
                    }
                    _ => return Err(ParsingError::WrongField(field)),
                }
                Ok(())
            }
        }

        impl TryFrom<PassportBuilder> for Passport {
            type Error = ParsingError;

            fn try_from(builder: PassportBuilder) -> Result<Self, Self::Error> {
                // We first try to get every field before trying to convert them
                // because we want to seperate passports missing a field from the
                // ones having a bad value. This makes the first part work.
                let byr_raw = builder.byr.ok_or(Self::Error::MissingField("byr"))?;
                let iyr_raw = builder.iyr.ok_or(Self::Error::MissingField("iyr"))?;
                let eyr_raw = builder.eyr.ok_or(Self::Error::MissingField("eyr"))?;
                let hgt_raw = builder.hgt.ok_or(Self::Error::MissingField("hgt"))?;
                let hcl_raw = builder.hcl.ok_or(Self::Error::MissingField("hcl"))?;
                let ecl_raw = builder.ecl.ok_or(Self::Error::MissingField("ecl"))?;
                let pid_raw = builder.pid.ok_or(Self::Error::MissingField("pid"))?;
                let cid_raw = builder.cid;

                // The leading zeros need to be still there
                if pid_raw.len() != 9 {
                    return Err(Self::Error::InvalidValue(pid_raw, "not of length 9"));
                }

                let passport = Self {
                    byr: byr_raw.parse()?,
                    iyr: iyr_raw.parse()?,
                    eyr: eyr_raw.parse()?,
                    hgt: hgt_raw.parse()?,
                    hcl: hcl_raw.parse()?,
                    ecl: ecl_raw.parse()?,
                    pid: pid_raw.parse()?,
                    cid: if let Some(cid) = cid_raw {
                        Some(cid.parse()?)
                    } else {
                        None
                    },
                };

                if passport.byr < 1920 || passport.byr > 2002 {
                    return Err(ParsingError::InvalidValue(
                        passport.byr.to_string(),
                        "not in range 1920 to 2002",
                    ));
                }

                if passport.iyr < 2010 || passport.iyr > 2020 {
                    return Err(ParsingError::InvalidValue(
                        passport.iyr.to_string(),
                        "not in range 2010 to 2020",
                    ));
                }

                if passport.eyr < 2020 || passport.eyr > 2030 {
                    return Err(ParsingError::InvalidValue(
                        passport.eyr.to_string(),
                        "not in range 2020 to 2030",
                    ));
                }

                match passport.hgt {
                    Height::Cm(size) => {
                        if size < 150 || size > 193 {
                            return Err(ParsingError::InvalidValue(
                                size.to_string(),
                                "not in range 150 to 193",
                            ));
                        }
                    }
                    Height::Inches(size) => {
                        if size < 59 || size > 76 {
                            return Err(ParsingError::InvalidValue(
                                size.to_string(),
                                "not in range 59 to 76",
                            ));
                        }
                    }
                }

                Ok(passport)
            }
        }

        let mut builder = PassportBuilder::default();

        let mut chars = s.chars().peekable();
        while chars.peek().is_some() {
            let field: String = chars.by_ref().take(3).collect();
            chars.next();
            let value: String = chars
                .by_ref()
                .take_while(|c| !c.is_ascii_whitespace())
                .collect();
            builder.add_field(field, value)?;
        }

        Passport::try_from(builder)
    }
}

impl Problem for Four {
    const YEAR: u32 = 2020;
    const DAY: u8 = 4;

    fn solve(input: String) -> (String, String) {
        let passports = input
            .trim()
            .split("\n\n")
            .map(|passport_string| Passport::from_str(passport_string))
            .collect::<Vec<_>>();

        let complete_passports = passports
            .iter()
            .filter(|p| !matches!(p, Err(ParsingError::MissingField(_))))
            .count();

        let valid_passports = passports.iter().filter(|p| p.is_ok()).count();

        (complete_passports.to_string(), valid_passports.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 208.to_string());
        assert_eq!(output.1, 167.to_string());
    }
}
