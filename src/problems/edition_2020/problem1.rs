use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct One;

impl Problem for One {
    const YEAR: u32 = 2020;
    const DAY: u8 = 1;

    fn solve(input: String) -> (String, String) {
        let numbers: Vec<_> = input
            .trim()
            .split('\n')
            .map(|sub_str| sub_str.parse::<u32>().unwrap())
            .collect();

        let mut answer1 = String::new();
        let mut answer2 = String::new();
        for (i, n1) in numbers.iter().enumerate() {
            for n2 in numbers[i + 1..].iter() {
                if n1 + n2 == 2020 {
                    answer1 = (n1 * n2).to_string();
                }

                for n3 in numbers[i + 2..].iter() {
                    if n1 + n2 + n3 == 2020 {
                        answer2 = (n1 * n2 * n3).to_string();
                    }
                }
            }
        }

        (answer1, answer2)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 902451.to_string());
        assert_eq!(output.1, 85555470.to_string());
    }
}
