use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Two;

struct Password {
    left_num: u8,
    right_num: u8,
    letter: char,
    password: String,
}

impl Password {
    fn from_str(s: &str) -> Self {
        let mut s = s.splitn(2, '-');
        let left_num = s.next().unwrap().parse::<u8>().unwrap();
        let mut s = s.next().unwrap().splitn(2, ' ');
        let right_num = s.next().unwrap().parse::<u8>().unwrap();
        let mut s = s.next().unwrap().splitn(2, ": ");
        let letter = s.next().unwrap().chars().next().unwrap();
        let password = s.next().unwrap().to_string();

        Self {
            left_num,
            right_num,
            letter,
            password,
        }
    }

    fn is_valid1(&self) -> bool {
        let count = self.password.matches(self.letter).count() as u8;

        self.left_num <= count && count <= self.right_num
    }

    fn is_valid2(&self) -> bool {
        let mut valid_letters = 0;
        if self
            .password
            .chars()
            .nth(self.left_num as usize - 1)
            .unwrap()
            == self.letter
        {
            valid_letters += 1;
        }
        if self
            .password
            .chars()
            .nth(self.right_num as usize - 1)
            .unwrap()
            == self.letter
        {
            valid_letters += 1;
        }

        valid_letters == 1
    }
}

impl Problem for Two {
    const YEAR: u32 = 2020;
    const DAY: u8 = 2;

    fn solve(input: String) -> (String, String) {
        let valid1 = input
            .trim()
            .split('\n')
            .map(|sub_str| Password::from_str(sub_str))
            .filter(|password| password.is_valid1())
            .count();

        let valid2 = input
            .trim()
            .split('\n')
            .map(|sub_str| Password::from_str(sub_str))
            .filter(|password| password.is_valid2())
            .count();
        (valid1.to_string(), valid2.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 422.to_string());
        assert_eq!(output.1, 451.to_string());
    }
}
