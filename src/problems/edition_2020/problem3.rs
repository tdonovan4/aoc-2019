use crate::problems::problem::Problem;

#[aoc_macros::mark_problem]
pub struct Three;

struct Line<'a>(&'a str);

impl<'a> Line<'a> {
    fn is_tree(&self, mut x: usize) -> Option<bool> {
        // Repetition of the pattern because of arboreal genetics
        // and biome stability or something
        while x >= self.0.len() {
            x -= self.0.len();
        }
        self.0.chars().nth(x).map(|c| c == '#')
    }
}

struct Map<'a>(Vec<Line<'a>>);

impl<'a> Map<'a> {
    fn from_str(s: &'a str) -> Self {
        Self(s.trim().split('\n').map(|line| Line(line)).collect())
    }
}

impl<'a> Map<'a> {
    fn count_trees(&self, slope: (usize, usize)) -> u64 {
        let mut trees = 0;
        let mut x = 0;
        for i in (0..self.0.len()).step_by(slope.1) {
            if let Some(true) = self.0[i].is_tree(x) {
                trees += 1;
            }
            x += slope.0;
        }
        trees
    }
}

impl Problem for Three {
    const YEAR: u32 = 2020;
    const DAY: u8 = 3;

    fn solve(input: String) -> (String, String) {
        let map = Map::from_str(input.as_str());

        let s1 = map.count_trees((1, 1));
        let s2 = map.count_trees((3, 1));
        let s3 = map.count_trees((5, 1));
        let s4 = map.count_trees((7, 1));
        let s5 = map.count_trees((1, 2));

        (s2.to_string(), (s1 * s2 * s3 * s4 * s5).to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solution() {
        let output = TargetProblem::execute();
        assert_eq!(output.0, 173.to_string());
        assert_eq!(output.1, 4385176320u64.to_string());
    }
}
