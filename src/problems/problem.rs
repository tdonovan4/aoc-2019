use crate::data;

pub trait Problem {
    const YEAR: u32;
    const DAY: u8;
    fn solve(input: String) -> (String, String);
    fn execute() -> (String, String) {
        Self::solve(data::Input::new(Self::YEAR, Self::DAY).get().unwrap())
    }
}

pub trait Edition {
    fn execute(&self);
}
