use std::io::{self, prelude::*, stdin, stdout, Write};
use std::path::{Path, PathBuf};
use std::sync::{RwLock, RwLockWriteGuard};
use std::{
    collections::HashMap,
    fs::{self, File},
};

use directories::ProjectDirs;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ProjectDirError {
    #[error("Project dir not found")]
    NotFound,
    #[error("Error while creating the dir")]
    IO(#[from] io::Error),
}

fn get_project_dir() -> Result<PathBuf, ProjectDirError> {
    let project_dir =
        ProjectDirs::from("dev", "tdonovan", "aoc").ok_or(ProjectDirError::NotFound)?;
    let path = project_dir.data_local_dir().to_owned();
    // Create project directory if it doesn't exist
    fs::create_dir_all(&path)?;

    Ok(path)
}

#[derive(Error, Debug)]
pub enum TokenError {
    #[error("Project dir error")]
    ProjectDir(#[from] ProjectDirError),
    #[error("Error while interacting with file")]
    IO(#[from] io::Error),
}

pub struct Token;

impl Token {
    pub fn get_session_token() -> Result<String, TokenError> {
        let folder = get_project_dir()?;
        match Token::get_from_file(&folder) {
            Ok(token) => Ok(token),
            Err(_) => {
                print!("Enter token: ");
                stdout().flush()?;
                let mut token = String::new();
                stdin().read_line(&mut token).expect("Failed to read line");
                token = token.trim().to_string();
                let mut path = PathBuf::new();
                path.push(folder);
                path.push("token");
                let mut file = File::create(path)?;
                file.write_all(token.as_bytes())?;
                Ok(token)
            }
        }
    }

    fn get_from_file(folder: &Path) -> Result<String, TokenError> {
        let mut path = PathBuf::new();
        path.push(folder);
        path.push("token");

        let mut file = File::open(path)?;
        let mut content = String::new();
        file.read_to_string(&mut content)?;
        Ok(content.trim().to_string())
    }
}

#[derive(Debug, Serialize, Deserialize, Default)]
struct EditionData(HashMap<u8, String>);

impl EditionData {
    fn insert(&mut self, day: u8, value: String) {
        self.0.insert(day, value);
    }

    fn get(&self, day: u8) -> Option<&str> {
        self.0.get(&day).map(|s| s.as_str())
    }
}

#[derive(Debug, Serialize, Deserialize, Default)]
struct EditionsData(HashMap<u32, EditionData>);

impl EditionsData {
    pub fn get(&self, year: u32) -> Option<&EditionData> {
        self.0.get(&year)
    }

    pub fn get_or_insert(&mut self, year: u32) -> &mut EditionData {
        self.0.entry(year).or_default()
    }
}

#[derive(Error, Debug)]
pub enum CacheError {
    #[error("Error while unlocking RwLock")]
    Lock,
    #[error("Project dir error")]
    ProjectDir(#[from] ProjectDirError),
    #[error("Error while interacting with file")]
    IO(#[from] io::Error),
    #[error("Error while serializing or deserializing")]
    Serde(#[from] serde_json::error::Error),
}

static CACHE: Lazy<Cache> = Lazy::new(Cache::new);
struct Cache {
    data: RwLock<EditionsData>,
}

impl Cache {
    pub fn new() -> Self {
        let data = Self::get_from_file().unwrap_or_default();
        Self {
            data: RwLock::new(data),
        }
    }

    pub fn get_problem(&self, year: u32, day: u8) -> Result<Option<String>, CacheError> {
        let data = self.data.read().map_err(|_| CacheError::Lock)?;
        Ok(data
            .get(year)
            .map(|edition| edition.get(day).map(|s| s.to_owned()))
            .flatten())
    }

    pub fn insert_problem(&self, year: u32, day: u8, content: String) -> Result<(), CacheError> {
        let mut data = self.data.write().map_err(|_| CacheError::Lock)?;
        let edition = data.get_or_insert(year);
        edition.insert(day, content);
        Self::write_to_file(data)
    }

    fn write_to_file(data: RwLockWriteGuard<EditionsData>) -> Result<(), CacheError> {
        let mut path = PathBuf::new();
        let folder = get_project_dir()?;
        path.push(folder);
        path.push("input.json");
        let mut file = File::create(path)?;
        let serialized = serde_json::to_string(&*data)?;
        file.write_all(serialized.as_bytes())?;
        Ok(())
    }

    fn get_from_file() -> Result<EditionsData, CacheError> {
        let mut path = PathBuf::new();
        path.push(get_project_dir()?);
        path.push("input.json");

        let mut file = File::open(path)?;
        let mut content = String::new();
        file.read_to_string(&mut content)?;
        Ok(serde_json::from_str(content.as_str())?)
    }
}

#[derive(Error, Debug)]
pub enum InputError {
    #[error("Error with the cache")]
    Cache(#[from] CacheError),
    #[error("Error while fetching the problem from the website")]
    Fetch(#[from] reqwest::Error),
    #[error("Error while getting the token")]
    Token(#[from] TokenError),
}

pub struct Input {
    year: u32,
    day: u8,
}

impl Input {
    pub fn new(year: u32, day: u8) -> Input {
        Input { year, day }
    }

    pub fn get(&self) -> Result<String, InputError> {
        if let Some(problem) = CACHE.get_problem(self.year, self.day)? {
            Ok(problem)
        } else {
            let problem = self.fetch()?;
            CACHE.insert_problem(self.year, self.day, problem.clone())?;
            Ok(problem)
        }
    }

    fn fetch(&self) -> Result<String, InputError> {
        let token = Token::get_session_token()?;
        let client = reqwest::blocking::Client::builder()
            .cookie_store(true)
            .build()?;
        Ok(client
            .get(
                format!(
                    "https://adventofcode.com/{}/day/{}/input",
                    self.year, self.day
                )
                .as_str(),
            )
            .header("Cookie", format!("session={}", token))
            .send()?
            .text()?)
    }
}
