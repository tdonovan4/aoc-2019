pub fn find_gcd(mut first: u64, mut second: u64) -> u64 {
    while second != 0 {
        let temp = second;
        second = first % second;
        first = temp;
    }

    first
}

pub fn find_lcm(first: u64, second: u64) -> u64 {
    if first == 0 {
        second
    } else if second == 0 {
        first
    } else {
        let gcd = find_gcd(first, second);
        first * second / gcd
    }
}
