use std::iter::Iterator;
use std::vec::IntoIter;

#[derive(Clone)]
pub enum State {
    Running,
    Wait,
    Halt((Vec<i64>, Vec<i64>)),
}

impl State {
    pub fn unwrap(self) -> (Vec<i64>, Vec<i64>) {
        match self {
            State::Halt(inside) => inside,
            _ => panic!("State isn't Halt"),
        }
    }
}

struct Intcode {
    code: u32,
    parameters: Vec<u32>,
}

impl Intcode {
    fn new(code: u32, parameters: Vec<u32>) -> Intcode {
        Intcode { code, parameters }
    }

    fn get_intcode(&self) -> u32 {
        self.code
    }

    fn get_value(&self, intcode: &[i64], idx: usize, offset: usize, relative_base: usize) -> i64 {
        let pos = idx + offset;
        match self.parameters.get(offset - 1) {
            Some(0) | None => intcode[intcode[pos] as usize],
            Some(1) => intcode[pos],
            Some(2) => intcode[(relative_base as i64 + intcode[pos]) as usize],
            _ => panic!("Wrong parameter"),
        }
    }

    fn get_destination(
        &self,
        intcode: &[i64],
        idx: usize,
        offset: usize,
        relative_base: usize,
    ) -> usize {
        let pos = idx + offset;
        match self.parameters.get(offset - 1) {
            Some(0) | None => intcode[pos] as usize,
            Some(2) => (relative_base as i64 + intcode[pos]) as usize,
            _ => panic!("Wrong parameter"),
        }
    }
}

pub struct IntcodeComputer {
    input: IntoIter<i64>,
    output: Vec<i64>,
    intcode: Vec<i64>,
    index: usize,
    relative_base: usize,
    state: State,
}

impl IntcodeComputer {
    fn get_input(&mut self) -> Option<i64> {
        self.input.next()
    }

    pub fn new(mut intcode: Vec<i64>, input: Option<Vec<i64>>) -> IntcodeComputer {
        let input = input.unwrap_or_default();
        if intcode.len() < 128 {
            intcode.resize(128, 0);
        } else {
            intcode.resize(intcode.len() * 2, 0)
        }

        IntcodeComputer {
            input: input.into_iter(),
            output: Vec::new(),
            intcode,
            index: 0,
            relative_base: 0,
            state: State::Running,
        }
    }
    fn parse_intcode(value: u32) -> Intcode {
        let mut split = value
            .to_string()
            .chars()
            .rev()
            .filter_map(|x| x.to_digit(10))
            .collect::<Vec<u32>>();
        let mut intcode = split.remove(0).to_string();
        if !split.is_empty() {
            let int = split.remove(0);
            if int > 0 {
                intcode.insert(0, std::char::from_digit(int as u32, 10).unwrap());
            }
        }
        Intcode::new(intcode.parse::<u32>().unwrap(), split)
    }
    fn get_value(&self, intcode: &Intcode, offset: usize) -> i64 {
        intcode.get_value(&self.intcode[..], self.index, offset, self.relative_base)
    }
    fn get_destination(&self, intcode: &Intcode, offset: usize) -> usize {
        intcode.get_destination(&self.intcode[..], self.index, offset, self.relative_base)
    }
    fn run_opcode(&mut self) {
        let result = IntcodeComputer::parse_intcode(self.intcode[self.index] as u32);
        match result.get_intcode() {
            1 => {
                let left = self.get_value(&result, 1);
                let right = self.get_value(&result, 2);
                let target = self.get_destination(&result, 3);
                self.intcode[target] = left + right;
                self.index += 4;
            }
            2 => {
                let left = self.get_value(&result, 1);
                let right = self.get_value(&result, 2);
                let target = self.get_destination(&result, 3);
                self.intcode[target] = left * right;
                self.index += 4;
            }
            3 => {
                let destination = self.get_destination(&result, 1);
                match self.get_input() {
                    Some(input) => {
                        self.intcode[destination] = input;
                        self.index += 2;
                    }
                    None => self.state = State::Wait,
                }
            }
            4 => {
                self.output.push(self.get_value(&result, 1));
                self.index += 2;
            }
            5 => {
                let condition = self.get_value(&result, 1);
                if condition != 0 {
                    self.index = self.get_value(&result, 2) as usize;
                } else {
                    self.index += 3;
                }
            }
            6 => {
                let condition = self.get_value(&result, 1);
                if condition == 0 {
                    self.index = self.get_value(&result, 2) as usize;
                } else {
                    self.index += 3;
                }
            }
            7 => {
                let left = self.get_value(&result, 1);
                let right = self.get_value(&result, 2);
                let value = if left < right { 1 } else { 0 };
                let destination = self.get_destination(&result, 3);
                self.intcode[destination] = value;
                self.index += 4;
            }
            8 => {
                let left = self.get_value(&result, 1);
                let right = self.get_value(&result, 2);
                let value = if left == right { 1 } else { 0 };
                let destination = self.get_destination(&result, 3);
                self.intcode[destination] = value;
                self.index += 4;
            }
            9 => {
                let value = self.get_value(&result, 1);
                self.relative_base = (self.relative_base as i64 + value) as usize;
                self.index += 2;
            }
            99 => {
                self.state = State::Halt((self.intcode.clone(), self.output.clone()));
            }
            _ => panic!("Wrong opcode"),
        }
    }
    pub fn compute(&mut self) -> State {
        self.state = State::Running;
        while let State::Running = self.state {
            self.run_opcode()
        }
        self.state.clone()
    }
    pub fn input(&mut self, mut new_input: Vec<i64>) {
        let mut input: Vec<i64> = Vec::from(self.input.as_slice());
        input.append(&mut new_input);
        self.input = input.into_iter();
    }
    pub fn get_output(&self) -> &Vec<i64> {
        &self.output
    }
    pub fn clear_output(&mut self) {
        self.output = Vec::new();
    }
}
